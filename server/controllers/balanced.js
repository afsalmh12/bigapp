const express = require("express");
const router = express.Router();
const db_balanced = require('../db_layer/db_balanced');
var Promise = require('promise');
//open and closed mapped using associative array
const paranthesisMap = [];
paranthesisMap['{'] = '}';
paranthesisMap['('] = ')';
paranthesisMap['['] = ']';

//function to check input parenthesis is balanced or not
function isBalanced(inputString) {
    return new Promise(function (resolve, reject) {
        var flag = false;
        var str = '{}[]()';
        if (inputString) {
            var expression = inputString.split('');
            var stack = [];
            for (var i = 0; i < expression.length; i++) {
                //checking charecter is a paranthesis
                if (str.indexOf(expression[i]) > -1) {
                    //checking charecter is a open paranthesis and if yes push to stack
                    if (paranthesisMap[expression[i]]) {
                        stack.push(expression[i]);
                    } else {
                        //closed paranthesis
                        if (stack.length === 0) {
                            resolve(false);
                        }
                        var top = stack.pop(); // pop off the top element from stack
                        //comparing closed paranthesis with top of stack
                        if (!(paranthesisMap[top] && paranthesisMap[top] == expression[i])) {
                            resolve(false);
                        }
                    }
                }
            }
            flag = stack.length > 0 ? false : true;
        }
        resolve(flag);
    });
}

router.get('/verifyParanthesis', function (req, res) {
    async function verifyParanthesis() {
        let email = req._id;
        let inputString = req.query.paranthesis;
        let flag = await isBalanced(inputString);
        let state = "Failed";
        if (flag) {
            state = "Success";
            var obj = {
                email: email,
                inputString: inputString,
                message: state
            }
            //saving to db if string is balanced
            await db_balanced.saveBalanced(obj);
        }
        //checking current attept number of the user
        var currentAttempts = await db_balanced.getAttamptNumber(email);
        if (currentAttempts != null) {
            currentAttempts++;
            //updating attempt number in db
            await db_balanced.updateAttempts(email, currentAttempts);
        } else {
            currentAttempts = 1 ;
            var attemptObj = {email:email,totalAttempts:currentAttempts}
            await db_balanced.addAttempt(attemptObj);
        }

        res.json({ userName: email, message :state ,attempts:currentAttempts});
    }
    verifyParanthesis();
});
module.exports = router;