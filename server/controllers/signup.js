const express = require("express");
const router = express.Router();
const bcrypt = require('bcrypt');
const Promise = require("promise");
const userSignUp = require('../db_layer/db_user');
const saltRounds = 10;

//encrypting password using bcrypt
function encrypt(password) {
    return new Promise(function (resolve, reject) {
        bcrypt.hash(password, saltRounds, function (err, hash) {
            if(err) reject(err);
            else resolve(hash);
        });
    });
}
router.post('/signup', function (req, res) {
    let email = req.query.email;
    let fullName = req.query.fullName;
    let dob = req.query.dob;
    let password = req.query.password;
    let role = req.query.role;
    async function signup() {
        if (email && fullName && role && password) {
            var user = {
                email: email,
                fullName: fullName,
                role: role,
                password: password,
                dob: ""
            }
            user.dob = dob ? dob : "N/A";
            //check if the email is exist or not 
            let isExistingEmail = await userSignUp.validEmail(email);
            if (!isExistingEmail) {
                user.password = await encrypt(user.password);
                //saving user details to db
                let result = await userSignUp.addUser(user);
                res.json(result);
            } else {
                res.send('Existing email id');
            }
        } else {
            res.send("Please provide complete user details");
        }
    }
    signup();
});
module.exports = router;