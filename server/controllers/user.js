const express = require("express");
const router = express.Router();
const db_user = require('../db_layer/db_user');
var Promise = require('promise');
router.get('/getAllUsers', function (req, res) {
    async function getAllUsers() {
        let email = req._id;
        //checking the logged user is admin or not
        let flag = await db_user.isAdmin(email);
        if (flag) {
            //calling existing users list 
            let usersList = await db_user.getAllUsers();
            res.json(usersList);
        } else {
            res.send("Unauthorized action");
        }
    }
    getAllUsers();
})
router.get('/removeUser', function (req, res) {
    async function removeUser() {
        let email = req._id;
        let userToRemove = req.query.email;
         //checking the logged user is admin or not
        let flag = await db_user.isAdmin(email);
        if (flag) {
            //remove given user from db
            var state = await db_user.removeUser(userToRemove);
            if (state) {
                res.send(userToRemove + " Successfully removed")
            } else {
                res.send("Failed");
            }
        } else {
            res.send("Unauthorized action");
        }
    }
    removeUser();
})
module.exports = router;
