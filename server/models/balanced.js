const mongoose = require('mongoose');
const schema = mongoose.Schema;
//defining balanced collection schema
const balancedSchema = new schema({
    email:String,
    inputString:String,
    message:String
},{
    versionKey: false 
})
module.exports = mongoose.model("balanced",balancedSchema,"balanced");
