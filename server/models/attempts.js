const mongoose = require('mongoose');
const schema = mongoose.Schema;
const attemptsSchema = new schema({
    email:String,
    totalAttempts:Number
},{
    versionKey: false 
})
module.exports = mongoose.model("attempts",attemptsSchema,"attempts");
