var jwt = require('jsonwebtoken');
const dotenv = require('dotenv').config();
module.exports.verifyJwtToken = (req,res,next) => {
    var token;
    //get token from header
    token = req.headers['authorization'];
    if(!token)
        return res.status(403).send({auth:false,message:"No token provided"});
    else{
        jwt.verify(token,process.env.superKey,(err,decoded)=>{
            if(err)
            return res.status(500).send({auth:false,message:"Invalid Token"});
            else{
                req._id = decoded.id;
                next();
            }
        })
    }
}