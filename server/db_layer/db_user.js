const user = require('../models/user');
const Promise = require("promise");
var bcrypt = require('bcrypt');
//function to verify password
function decrypt(hash,password){
   return new Promise(function (resolve, reject) {
        bcrypt.compare(password, hash, function(err, res) {
            if(err) reject(err)
            else resolve(res)
        });
    });
}

//function to check the given email is exist
function validEmail(emailId) {
    return new Promise(function (resolve, reject) {
        user.findOne({ email: emailId }, '_id email').exec(
            function (err, result) {
                let flag = false;
                if (err) reject(err);
                else {
                    console.log(result);
                    if (result) {
                        flag = true;
                    }
                    resolve(flag);
                }
            })
    })
}

//function to save user detials in db
function addUser(userMdl) {
    return new Promise(function (resolve, reject) {
        let newUser = new user(userMdl);
        newUser.save()
            .then((data) => {
                console.log(data);
                resolve("Success")
            })
            .catch((err) => {
                console.log(err);
                reject(err);
            })
    })
}

//function to verify user credentials 
function validUser(emailId, password) {
    return new Promise(function (resolve, reject) {
        user.findOne({ email: emailId }, 'email password fullName').exec(
            async function (err, result) {
                let result1 = false;
                if (err) reject(err);
                else {
                    if (result) {
                        var verified = await decrypt(result.password,password);
                        //console.log(verified);
                        if (verified) {
                            result1 = result;
                        }
                    }
                    resolve(result1);
                }
            })
    })
}
//function to list all users in db
function getAllUsers(){
    return new Promise(function (resolve, reject) {
        user.find({ }, 'email').exec(
            function (err, result) {
                if (err) reject(err);
                else {
                    resolve(result);
                }
            })
    })
}


//function to check logged user is admin or not
function isAdmin(emailId){
    return new Promise(function (resolve, reject) {
        user.findOne({ email: emailId }, 'role').exec(
            function (err, result) {
                let flag = false;
                if (err) reject(err);
                else {
                    if (result && result.role && result.role == "Admin") {
                        flag = true;
                    }
                    resolve(flag);
                }
            })
    })
}
//function to remove user
function removeUser(emailId){
    return new Promise(function (resolve, reject) {
        user.deleteOne({ email: emailId }).exec(
            function (err, result) {
                if (err) reject(err);
                else {
                    resolve(true);
                }
            })
    })
}
module.exports = { 
'validEmail': validEmail ,
'addUser':addUser ,
'validUser':validUser,
'getAllUsers':getAllUsers,
'isAdmin':isAdmin,
'removeUser':removeUser
};